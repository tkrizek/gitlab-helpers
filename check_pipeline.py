#!/usr/bin/python3

from pprint import pprint
import logging
import sys

import gitlab

def main():
    logging.basicConfig(level=logging.WARNING, format='%(message)s')
    pipeid = int(sys.argv[1])

    gl = gitlab.Gitlab.from_config('isc', ['/home/pspacek/w/.python-gitlab.cfg'])
    project = gl.projects.get('isc-projects%2Fbind9')

    oss_proj = gl.projects.get(1)

    pipe = oss_proj.pipelines.get(pipeid)
    return pipe

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    pipe = main()
