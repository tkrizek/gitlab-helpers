#!/usr/bin/python3

import time
import sys

import gitlab

gl = gitlab.Gitlab.from_config()
oss_proj = gl.projects.get(1, lazy=True)
pipe = oss_proj.pipelines.get(sys.argv[1])

states_fail = {'failed', 'canceled', 'skipped'}
states_done = {'failed', 'success', 'canceled', 'skipped'}

while True:
    bridges = pipe.bridges.list(all=True)
    assert len(bridges) > 0
    if any(bridge.downstream_pipeline['status'] in states_fail
           for bridge in bridges
           if bridge.downstream_pipeline):
        print('FAILURE DETECTED!')
        break
    if all(bridge.downstream_pipeline and bridge.downstream_pipeline['status'] in states_done
           for bridge in bridges):
        print('success')
        break
    for bridge in bridges:
        print(bridge.downstream_pipeline)
    print('not done, waiting ...')
    time.sleep(30)
#for bridge in bridges:
#    print(bridge.downstream_pipeline)
