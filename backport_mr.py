#!/usr/bin/python3

import argparse
import git

from pprint import pprint
import logging
import sys
import shlex

import gitlab

REPO_FS_PATH = '/home/pspacek/w/pkg/bind/git'

label2branch = {
        'v9.19': 'main',
        'v9.18': 'bind-9.18',
        'v9.18-S': 'bind-9.18-sub',
        'v9.17': None,
        'v9.16': 'bind-9.16',
        'v9.16-S': 'bind-v9.16-sub',
        'v9.11 (EoL)': 'v9_11',
        'v9.11-S': 'v9_11_sub',
        }

branch2label = {branch: label for label, branch in label2branch.items()}

def backport_to_branches(target_branch, labels):
    return list(label2branch[label] for label in labels if label in label2branch and label2branch[label] != target_branch)

def get_remote_name(local_branch):
    repo = git.Repo(REPO_FS_PATH)
    branch = repo.branches[local_branch]
    remote_ref = branch.tracking_branch()
    return remote_ref.remote_name

def create_backport_mr(source_mr, target_branch):
    omit_labels = ['LGTM (Merge OK)', 'Review']
    omit_labels.extend(label2branch.keys())
    omit_labels.extend('Affects {0}'.format(v) for v in label2branch.keys())

    assert target_branch in branch2label

    create_params = {
            'target_branch': target_branch,
            'remove_source_branch': True,
            'allow_collaboration': True,
            'milestone_id': source_mr.milestone['id'],
            'milestone_title': source_mr.milestone['title'],
            'assignee_id': source_mr.assignee and source_mr.assignee['id'],
        }
    create_params['title'] = f'[{target_branch[5:]}] {source_mr.title}'
    create_params['labels'] = list(label for label in source_mr.labels if label not in omit_labels) \
            + ['Backport', branch2label[target_branch]]
    create_params['description'] = f'Backport of MR !{source_mr.iid}'
    return create_params

def cherrypick_cmd(source_mr):
    commits = list(source_mr.commits())
    # API returns commits in reverse order (topmost/latest goes first)
    first_commit = commits[-1]
    last_commit = commits[0]
    return f'git cherry-pick -x {first_commit.id}^..{last_commit.id}'

def push_cmd(params):
    opts = []
    opts.extend(['-o', 'merge_request.create'])
    opts.extend(['-o', f'merge_request.target={params["target_branch"]}'])
    opts.extend(['-o', f'merge_request.title={params["title"]}'])
    opts.extend(['-o', f'merge_request.milestone={params["milestone_title"]}'])
    opts.extend(['-o', f'merge_request.description={params["description"]}'])
    for label in params['labels']:
        opts.extend(['-o', f'merge_request.label={label}'])
    return opts

    #from IPython.core.debugger import set_trace
    #set_trace()



def cmds(source_mr, target_branch: str, target_remote: str):
    print(f'# {target_branch}')
    backport_branch = f'{source_mr.source_branch}-{target_branch[5:]}'
    wrkpath = f'/tmp/{backport_branch.replace("/", "_")}'
    print(f"git fetch -a {target_remote}")
    print(f"git worktree add -b {backport_branch} {wrkpath} {target_remote}/{target_branch}")
    print(f"pushd {wrkpath}")
    print(cherrypick_cmd(source_mr))
    params = create_backport_mr(source_mr, target_branch)
    print(f'git push {target_remote}', " ".join(shlex.quote(el) for el in push_cmd(params)))
    print(f"popd")
    print(f"git worktree remove {wrkpath}")

def main():
    logging.basicConfig(level=logging.WARNING, format='%(message)s')

    parser = argparse.ArgumentParser(description='generate commands for backports')
    parser.add_argument('mrid', type=int,
                        help='MR # to be backported')
    parser.add_argument('--priv', dest='private', action='store_true',
                        default=False,
                        help='read MR data from the private repo')
    args = parser.parse_args()

    gl = gitlab.Gitlab.from_config('isc', ['/home/pspacek/w/.python-gitlab.cfg'])
    if args.private:
        proj = gl.projects.get('isc-private/bind9')
    else:
        proj = gl.projects.get('isc-projects/bind9')

    print(proj.path_with_namespace)
    mr = proj.mergerequests.get(args.mrid)
    assert mr.state == 'merged'

    logging.info('MR labels: %s', mr.labels)
    if 'Backport' in mr.labels:
        raise ValueError('MR is backport, use parent MR id')
    if mr.state != 'merged':
        raise ValueError('MR is not merged yet, merge it first')

    backport_to = backport_to_branches(mr.target_branch, mr.labels)
    logging.info('To be backported to: %s', backport_to)
    for target_branch in backport_to:
        remote_name = get_remote_name(target_branch)
        cmds(mr, target_branch, remote_name)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    mr = main()
