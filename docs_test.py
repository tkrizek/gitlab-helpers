#!/usr/bin/python3
'''
Start pipelines with only pylint & flake8 jobs.
Wait until all jobs success or fail.
Intended for use with staging images with new Python QA tools.
'''

import logging
import sys
import time

import gitlab

staging_image = [{'key': 'CI_REGISTRY_IMAGE',
                  'value': 'registry.gitlab.isc.org/isc-projects/images/bind9-staging'}]


def print_job(proj, job):
    print(proj.path_with_namespace, job.ref, job.name, job.web_url, job.status)

def print_jobs(loglevel, heading, projjoblist):
    if len(projjoblist) > 0:
        logging.log(loglevel, '%s:', heading)
        for proj, job in projjoblist:
            print_job(proj, job)

def main():
    gl = gitlab.Gitlab.from_config()
    gl.timeout = 30  # is really pipeline start that slow?
    oss_project = gl.projects.get('isc-projects/bind9')
    private_project = gl.projects.get('isc-private/bind9')

    jobs = []  # tuple: (project, job)
    for proj in (oss_project, private_project):
        try:
            pipe = proj.pipelines.get(sys.argv[1])
        except gitlab.exceptions.GitlabGetError as ex:
            print(ex)
            continue
        break

    assert pipe
    logging.info('got pipeline %d', pipe.id)

    for job in pipe.jobs.list(as_list=False):
        if job.name not in {'autoreconf', 'tarball-create', 'docs', 'docs:tarball'}:
            logging.debug('canceling job %d %s', job.id, job.name)
            proj.jobs.get(job.id, lazy=True).cancel()
        else:
            logging.info('adding job %s %d in project %s to watch list',
                         job.name, job.id, proj.path_with_namespace)
            jobs.append((proj, proj.jobs.get(job.id, lazy=True)))


    # debug: jobs.append((private_project, private_project.jobs.get(2173043, lazy=True)))
    # docs: states_not_done = {'created', 'pending', 'running', 'manual'}
    states_done = {'failed', 'success', 'canceled', 'skipped'}
    states_success = {'success'}

    jobs_failed = []
    jobs_succeeded = []

    retry_delay = 30
    while len(jobs) >= 1:
        logging.info('waiting for %d jobs to finish, waiting %d seconds', len(jobs), retry_delay)
        time.sleep(retry_delay)
        just_finished = []
        for proj, job in jobs:
            job.refresh()
            if job.status in states_done:
                if job.status in states_success:
                    jobs_succeeded.append((proj, job))
                else:
                    jobs_failed.append((proj, job))
                just_finished.append((proj, job))

        # clean up finished jobs
        for proj_job in just_finished:
            jobs.remove(proj_job)

    print_jobs(logging.INFO, 'jobs succeeded', jobs_succeeded)
    print_jobs(logging.CRITICAL, 'jobs failed', jobs_failed)
    if len(jobs_failed) == 0:
        print('Astounding success!')
    else:
        sys.exit(1)

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format='%(levelname)s %(message)s')
    main()
