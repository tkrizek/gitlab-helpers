#!/usr/bin/python3

import os
from pprint import pprint
import logging
import sys

import gitlab

def main():
    logging.basicConfig(level=logging.WARNING, format='%(message)s')
    pipeid = int(sys.argv[1])

    gl = gitlab.Gitlab.from_config('isc', ['/home/pspacek/w/.python-gitlab.cfg'])
    project = gl.projects.get('isc-projects/bind9')

    oss_proj = gl.projects.get(1)

    pipe = oss_proj.pipelines.get(pipeid)
    for job in pipe.jobs.list(all=True):
        print(f'{job.name} {job.status}')
        if job.status == 'success':
            os.mkdir(job.name)
            j = oss_proj.jobs.get(job.id)
            artifacts = j.artifacts()
            with open(f'{job.name}/artifacts.zip', 'wb') as zipf:
                zipf.write(artifacts)
    return pipe

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    pipe = main()
