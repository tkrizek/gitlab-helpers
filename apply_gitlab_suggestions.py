#!/usr/bin/python3

import os.path
import argparse
import re

import git

from pprint import pprint
import logging
import sys
import shlex

import gitlab

REPO_FS_PATH = "/home/pspacek/w/pkg/bind/wrktree-sugg"


def get_suggestions(mr):
    for note in mr.notes.list(as_list=False):
        if note.type != "DiffNote":
            continue
        # note.pprint()
        if not note.resolvable or note.resolved:
            continue
        if "```suggestion:-" not in note.body:
            continue
        yield note


def parse_suggestion(note):
    assert note.position["position_type"] == "text", "only text notes supported"
    assert (
        note.position["old_path"] == note.position["new_path"]
    ), "renames not supported"
    suggestions = list(
        re.finditer(
            "```suggestion:-(?P<minus>[0-9]+)\+(?P<plus>[0-9]+)\n(?P<newtext>.*?)\n```(\n|$)",
            note.body,
            flags=re.DOTALL,
        )
    )
    assert len(suggestions) == 1
    suggestion = suggestions[0]
    minus_off = int(suggestion.group("minus"))
    plus_off = int(suggestion.group("plus"))
    # is it possible we would not have a new line index?
    start = note.position["new_line"]
    newtext = suggestion.group("newtext")
    # it should not be a problem, but let's stay on the safe size
    assert "```" not in "newtext", "suggestions with ``` in text are not supported"

    remove_start = start - minus_off
    remove_end = start + plus_off
    assert remove_start >= 1
    assert remove_start <= remove_end
    return remove_start, remove_end, newtext


def find_orig_commit(repo, commit, path, start_line, end_line):
    # print(commit, path, start_line, end_line)
    line_sources = {}
    for source in repo.blame_incremental(rev=commit, file=path):
        start_intersection = max(start_line, source.linenos.start)
        end_intersection = min(end_line, source.linenos.stop - 1)
        for lineno in range(start_intersection, end_intersection + 1):
            # print(start_intersection, end_intersection, source)
            assert lineno not in line_sources, lineno
            line_sources[lineno] = source.commit
    return line_sources


def prepare_diff(repo, note):
    remove_start, remove_end, new_text = parse_suggestion(note)
    commit = repo.commit(note.position["head_sha"])
    path = note.position["new_path"]

    line_sources = find_orig_commit(repo, commit, path, remove_start, remove_end)

    repo.head.reset(commit=commit, working_tree=True, index=True)
    # list of lines, with dummy line 0 to start counting from 1
    commit_lines = [None] + (commit.tree / path).data_stream.read().decode(
        "utf-8"
    ).split("\n")
    new_lines = new_text.split("\n")
    merged = commit_lines[:remove_start] + new_lines + commit_lines[remove_end + 1 :]

    with open(os.path.join(repo.working_tree_dir, path), "w") as outf:
        outf.write("\n".join(merged[1:]))
    repo.index.add([path])
    diff = commit.diff(create_patch=True, paths=[path])
    return diff, line_sources


def commit_diff(repo, note, line_sources):
    line_commits = set(line_sources.values())
    # print(line_sources, line_commits)
    sugg_commit = repo.commit(note.position["head_sha"])
    if len(line_commits) == 1:
        # single source commit for all lines, that was, easy!
        orig_commit = line_commits.pop()
        orig_short_msg = orig_commit.message.split("\n")[0]
        msg = f'fixup! {orig_short_msg}\n\n{note.position["new_path"]} {note.position["new_line"]} {note.position["head_sha"]}'
    else:
        # more than one source commit for all lines, do not create fixup
        orig_short_msg = sugg_commit.message.split("\n")[0]
        msg = f'multi commit suggestion! {note.position["new_path"]} {note.position["new_line"]}\n\n{orig_short_msg} {note.position["head_sha"]} '
    print(repo.index.commit(msg))


def main():
    logging.basicConfig(level=logging.WARNING, format="%(message)s")

    parser = argparse.ArgumentParser(description="generate commands for backports")
    parser.add_argument("mrid", type=int, help="MR # to be backported")
    parser.add_argument(
        "--priv",
        dest="private",
        action="store_true",
        default=False,
        help="read MR data from the private repo",
    )
    args = parser.parse_args()

    gl = gitlab.Gitlab.from_config("isc", ["/home/pspacek/w/.python-gitlab.cfg"])
    if args.private:
        proj = gl.projects.get("isc-private/bind9")
    else:
        proj = gl.projects.get("isc-projects/bind9")

    mr = proj.mergerequests.get(args.mrid)
    if mr.state != "opened":
        print(mr)
        raise ValueError("MR is not open?!")

    local_repo = git.Repo(REPO_FS_PATH)
    for note in get_suggestions(mr):
        _diff, line_sources = prepare_diff(local_repo, note)
        commit_diff(local_repo, note, line_sources)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
